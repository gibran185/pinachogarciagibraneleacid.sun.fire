<?xml version="1.0" encoding="UTF-8"?>
<catalog>
<?php 
if (isset($entidades)){
foreach ($entidades as $b) { ?>
  <entidad id="<?php echo $b['clave']; ?>">
    
      <descripcion><?php echo $b['descripcion']; ?></descripcion>
   
  </entidad>
<?php }} ?>

<?php 
if (isset($municipios)){
foreach ($municipios as $b) { ?>
  <municipio id="<?php echo $b['clave']; ?>">
    
      <descripcion><?php echo $b['descripcion']; ?></descripcion>
   
  </municipio>
<?php }} ?>


<?php 
if (isset($indicadores)){
foreach ($indicadores as $b) { ?>
  <indicador id="<?php echo $b['clave']; ?>">
    
      <descripcion><?php echo $b['descripcion']; ?></descripcion>
   
  </indicador>
<?php }} ?>

<?php 
if (isset($temas)){
foreach ($temas as $b) { ?>
  <tema id="<?php echo $b['clave']; ?>">
    
      <descripcion><?php echo $b['descripcion']; ?></descripcion>
   
  </tema>
<?php }} ?>



<?php 
if (isset($montos)){
foreach ($montos as $b) { ?>
  <indicadores-monto id="<?php echo $b['clave']; ?>">
    
      <clave-entidad><?php echo $b['clave_entidad']; ?></clave-entidad>
	<clave-tema-nivel-1><?php echo $b['clave_tema_nivel1']; ?></clave-tema-nivel-1>
	<clave-tema-nivel-2><?php echo $b['clave_tema_nivel2']; ?></clave-tema-nivel-2>
	<clave-tema-nivel-3><?php echo $b['clave_tema_nivel3']; ?></clave-tema-nivel-3>
	<clave-indicador><?php echo $b['clave_indicador']; ?></clave-indicador>
	<anio><?php echo $b['anio']; ?></anio>
	<monto><?php echo $b['monto']; ?></monto>
	<unidad><?php echo $b['unidad']; ?></unidad>
   
  </indicadores-monto>
<?php }} ?>
</catalog>
