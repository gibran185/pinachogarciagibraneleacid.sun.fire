<?php
require '../../app/vendor/autoload.php';
$app = new \Slim\Slim();
$app->config(array(
    'templates.path' => '../../app/templates/'
));
$app->container->singleton('db', function () {
  return new PDO("pgsql:host=127.0.0.1 user=a10160866 password=1298439Ze dbname=a10160866 port=1111");
});

require '../../app/routes/root.php';
require '../../app/routes/get.php';
require '../../app/routes/post.php';
require '../../app/routes/delete.php';

$app->run();
?>


