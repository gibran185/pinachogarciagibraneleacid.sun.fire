<!DOCTYPE html>
  <head>
    <meta charset="utf-8" />
    <title>Inicio</title>
  </head>
  <body>
    <h1>PIB y sistema de cuentas nacionales de México</h1>
    <hr />
    <h2>Sobre la API</h2>
    <p>
       La API surge a partir de los datos de que se encuentran en la pagina del INEGI 
       <a href="http://www3.inegi.org.mx/sistemas/descarga/" >PIB y sistema de cuentas nacionales de México </a>
       Los datos estan resumidos de forma Nacional.
    </p>
    <p>
       Para información sobre como hacer uso de la API, <a href="doc/">acceda a la documentación</a>.
    </p>
    <hr />
    <h2>Integrantes</h2>
    <p>
     -López Ojeda Christina<br>
			-Mendiola Sarabio Bernardo<br>
			-Pinacho García Gibran Eleacid<br>
			-Morales Lorenzo Agustín<br>
    </p>
  </body>
</html>
